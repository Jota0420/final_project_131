'''
Author: Jose Daniel Orrego
Due date: April 27, 2021
Class: ISTA 131
Section Leader: Ales Waskow

Description:
Video Game Industry Growth

Credit to Abdo Khalf who came up with the idea
'''

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


df = pd.read_csv('/Users/danielmain/Desktop/Classes/CURRENT/ISTA 131/Final/repos/final_project_131/vgsales.csv')

rel_by_year = df.groupby('Year', as_index=False)['Name'].count().astype({'Year':'int'}).rename(columns={'Name':'count'})

sns.barplot(x='Year', y='count', data=rel_by_year)
plt.title('Video Games Released by Year', fontsize=30, pad = 20)
plt.xlabel('')
plt.ylabel('Number of Games Released', fontsize=28, labelpad = 20);
plt.xlim([19,38])
plt.yticks(fontsize=22)
plt.xticks(fontsize=22, rotation = -45, position = (0,-0.01))
plt.show()

