'''
Author: Jose Daniel Orrego
Due date: April 27, 2021
Class: ISTA 131
Section Leader: Ales Waskow

Description:
Video Game Industry Growth

Credit to Abdo Khalf who came up with the idea

Please check this on Jupyter Notebook as if you do it in something like VScode the 
xticks won't fit on the screen
'''

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv('/Users/danielmain/Desktop/Classes/CURRENT/ISTA 131/Final/repos/final_project_131/vgsales.csv')

top_publishers=df.groupby('Publisher', as_index=False)['Name'].count().sort_values('Name', ascending=False).rename(columns={'Name':'count'})

ax = sns.barplot(x='Publisher', y='count', data=top_publishers[:5], palette="viridis")
plt.title('Top 5 Publishers by Video Game Sales', fontsize=30, pad = 20)
plt.xlabel('')
plt.ylabel('Sales', fontsize=28, labelpad = 20)
plt.yticks(fontsize = 22)
plt.xticks(fontsize = 22)
for i in ax.patches:
    ax.text(i.get_x()+i.get_width()/2, i.get_height()+1, int(i.get_height()),  ha='center', va='bottom', fontsize = 20)
plt.show()