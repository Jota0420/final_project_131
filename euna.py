'''
Author: Jose Daniel Orrego
Date: April 27, 2021
Class: ISTA 131
Section Leader: Ales Waskow

Description:
Video Game Industry Growth

Please check this on Jupyter Notebook as if you do it in something like VScode the 
xticks won't fit on the screen
'''

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


df = pd.read_csv('/Users/danielmain/Desktop/Classes/CURRENT/ISTA 131/Final/repos/final_project_131/vgsales.csv')

sns.lmplot(x="NA_Sales",y="EU_Sales",data=df, line_kws={'color': 'red'})
plt.title("EU Sales & NA Sales", fontsize = 25, pad = 20)
plt.xlabel("NA Video Game Sales (millions)", fontsize = 20)
plt.ylabel("EU Video Game Sales (millions)", fontsize = 20)
plt.yticks(fontsize = 19)
plt.xticks(fontsize = 19)

plt.show()